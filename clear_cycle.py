#TODO args, kwargs везде
import config

from _walls import *
from _decorators import *
from _geometry import *
from _functions import *

def wall_handler(mebel_set, room_walls, wall, orient_key, abstract_object, 
                parent_abstract_object=None, corner_side='max'
                ):
    '''
    mebel_set, room_walls, wall, orient_key, abstract_object, corner_side='max', parent_abstract_object=None
    '''

    place_founded = False
    bbox = []
    x_y = ()

    length = length_helper()
    get_length = length[orient_key]
    concrete_objects = mebel_set.FilterByType(value=abstract_object)

    for concrete_obj in concrete_objects:

        if place_founded:
            break
        if get_length(wall) < concrete_obj.width: 
            #TODO добавить выбор стороны объекта
            pass

        else:
            place = config.PLACE[abstract_object]
            @decorators_generator(orientation=orient_key, walls=room_walls, obj=concrete_obj,
                        wall=wall, place=place, corner_side=corner_side,
                        abs_obj=parent_abstract_object
                        )
            def get_coordinates(*args, **kwargs):
                if kwargs['closed_wall']:
                    result = kwargs_handler(**kwargs)
                else:
                    result = set_none(n=4)
                print(result)
                return result
            
            result = get_coordinates(walls=room_walls, bbox=bbox, orient_key=orient_key, 
                                     x_y=x_y, closed_wall=None)
            closed_wall, orient_key, x_y, bbox = result
            place_founded = check_place_founded(closed_wall)

            if place_founded:
                save_results(abstract_object, concrete_obj, result)
                result = closed_wall, orient_key, bbox
            else:
                result = set_none(n=3)
            print(result)
            return result

def cycle(*args, **kwargs):
    """
    Собирает вместе все циклы для определения положения объекта
    mebel_set, abstract_object, room_walls, first_try=False, main_object=False, dependency_type=None, parent_abstract_object=None
    return: closed_wall, orient_key, bbox
    """
    mebel_set = kwargs.get('mebel_set', False)
    room_walls = kwargs.get('room_walls', False)
    abstract_object = kwargs.get('abstract_object', False)
    parent_abstract_object = kwargs.get('parent_abstract_object', None)
    main_object = kwargs.get('main_object', False)
    dependency_type = kwargs.get('dependency_type', None)
    first_try = kwargs.get('first_try', False)

    closed_wall = None
    if main_object:
        if first_try:
            orient_key, wall_index = find_largest_wall_parameters(room_walls)
            wall = room_walls[orient_key][wall_index]
            closed_wall, \
            orient_key, \
            bbox = wall_handler(
                                mebel_set, 
                                room_walls, 
                                wall, 
                                orient_key, 
                                abstract_object,
                                parent_abstract_object=parent_abstract_object
                                )
        else: 
            for orientation in list(room_walls.keys()):
                for wall in room_walls[orientation]:
                    if not closed_wall:
                        closed_wall, \
                        orient_key, \
                        bbox = wall_handler(
                                            mebel_set, 
                                            room_walls, 
                                            wall, 
                                            orientation, 
                                            abstract_object,
                                            parent_abstract_object=parent_abstract_object
                                            )
    else:
        if dependency_type is not 'both_side':
            for orientation in room_walls:
                for wall in room_walls[orientation]:
                    print(color.BLUE + abstract_object + color.END)
                    if not closed_wall:
                        closed_wall, orient_key, bbox = wall_handler(mebel_set, room_walls, wall, 
                                                                        orientation, abstract_object,
                                                                        parent_abstract_object=parent_abstract_object)
        else:
            for orientation in room_walls:
                for wall in room_walls[orientation]:
                    closed_wall, \
                    orient_key, \
                    bbox = wall_handler(
                                        mebel_set, 
                                        room_walls, 
                                        wall, 
                                        orientation, 
                                        abstract_object,
                                        corner_side='max',
                                        parent_abstract_object=parent_abstract_object
                                        )
                    
    return closed_wall, orient_key, bbox

def object_handler(*args, **kwargs):
    """
    mebel_set, abstract_object, room_walls, parent_abstract_object=None, main_object=False, dependency_type=None
    Returns:
        result = closed_wall, orient_key, bbox
    """
    #mebel_set = kwargs.get('mebel_set', False)
    room_walls = kwargs.get('room_walls', False)
    #abstract_object = kwargs.get('abstract_object', False)
    #parent_abstract_object = kwargs.get('parent_abstract_object', None)
    main_object = kwargs.get('main_object', False)
    #dependency_type = kwargs.get('dependency_type', None)

    #args = mebel_set, abstract_object, room_walls
    #kwargs = {"main_object":main_object, "dependency_type": dependency_type, "parent_abstract_object": parent_abstract_object}

    if main_object:
        result = cycle(first_try=True, *args, **kwargs)
        if not result:
            result = cycle(first_try=False, *args, **kwargs)

    else:
        result = cycle(first_try=False, *args, **kwargs)

    closed_wall, orient_key, bbox = result
    room_walls = recalculate_room_walls(room_walls, *result)
    closed_wall = bbox_proection(*result)
    result = room_walls, closed_wall, orient_key

    return result

def try_to_place_object(**kwargs):
    abstract_object = kwargs.get('abstract_object', False)
    placed_objects = list(config.FURN.keys())

    if abstract_object in placed_objects:
        placement_result = None, None, None
    else:
        placement_result = object_handler(main_object=True, **kwargs)
    
    return placement_result

def try_to_place_dependent(**kwargs):
    room_walls = kwargs.get('room_walls', False)
    closed_wall = kwargs.get('closed_wall', False)
    orient_key = kwargs.get('orient_key', False)
    abstract_object = kwargs.get('abstract_object', False)

    if abstract_object not in list(config.DEPENDENCE.keys()):
        return room_walls

    for dep_abstract_object in config.DEPENDENCE[abstract_object]:
        print(dep_abstract_object)

        dependency_type = config.DEPENDENCE[abstract_object][dep_abstract_object]
        if dependency_type not in ['near', 'opposite', 'in_front_of']:
            break

        availiable_walls = get_availiable_walls(room_walls, closed_wall, orient_key, abstract_object=abstract_object, dependency_type=dependency_type)
        residue, closed_wall, orient_key = object_handler(
                                    **dict(kwargs,
                                    abstract_object=dep_abstract_object, 
                                    room_walls=availiable_walls, 
                                    parent_abstract_object=abstract_object,
                                    main_object=False, 
                                    dependency_type=dependency_type))
        room_walls[orient_key] = residue[orient_key]
    
    return room_walls

def placement(*args, **kwargs):
    """
    furniture placement in one isolated room
    kwargs: mebel_set, room_walls, furnitrue_list
    """
    #mebel_set = kwargs.get('mebel_set', False)
    #room_walls = kwargs.get('room_walls', False)
    furniture_list = kwargs.get('furniture_list', False)

    for abstract_object in furniture_list: #TODO А для двух и более?
        placement_result = try_to_place_object(abstract_object=abstract_object, **kwargs)
        room_walls, closed_wall, orient_key = placement_result

        room_walls = try_to_place_dependent(abstract_object=abstract_object,
                                            closed_wall=closed_wall,
                                            orient_key=orient_key,
                                             **kwargs)


    
    return room_walls


"""
тестировочные штуки
room_walls = {'top': [((1505.0, 2499.0), (1673.0, 2499.0)), ((168.0, 3024.0), (595.0, 3024.0))], 'bottom': [((1673.0, 693.0), (350.0, 693.0)), ((343.0, 707.0), (168.0, 707.0)), ((1652.0, 2716.0), (1505.0, 2716.0))], 'left': [((168.0, 707.0), (168.0, 3024.0))], 'right': [((1505.0, 2716.0), (1505.0, 2499.0)), ((1652.0, 3024.0), (1652.0, 2716.0)), ((1673.0, 2499.0), (1673.0, 693.0))]}
plt.show()
"""