'''
Функции проверок возможности расположения объекта в некотором месте
'''
import math

from shapely import geometry

import config
import utils as u
import geometry as gmt
from helpers import color

def check_free_space(
    wall, 
    orient_key, 
    concrete_object
    ):
    '''
    Достаточно ли места для выбранного объекта у выбранной стены?
    return True/False
    '''
    obj_main_side = concrete_object.width #TODO left right top bottom
    if not wall:
        return False
    if orient_key == 'top' or orient_key == 'bottom':
        return obj_main_side < abs(wall[1][0] - wall[0][0])
    else:
        return obj_main_side < abs(wall[0][1] - wall[1][1])

def check_min_distance(
    bbox, # [[232.0, 322.0, 322.0, 232.0, 232.0], [90.5, 90.5, 290.5, 290.5, 90.5]]
    coef=20
    ):
    '''
    Проверяет расстояние от потенциального бокса до уже расположенных
    в комнате объектов
    True/False
    '''
    added_furniture = list(config.FURN.values())
    #added_furniture = [[[232.0, 322.0, 322.0, 232.0, 232.0], [90.5, 90.5, 290.5, 290.5, 90.5]]]
    potential_vec = u.bbox_to_vector(bbox)

    for box in added_furniture:
        added_box = u.bbox_to_vector(box)
        for a, b in u.standart_nested_loop(added_box, potential_vec):
            distance = gmt.euclidean_distance(a, b)
            #print(color.RED + 'Min distance: ' + str(distance) + color.END)
            if distance < coef:

                return False

    return True

#def another_walls_intersection()