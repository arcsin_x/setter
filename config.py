'''
Описание того, где какие предметы должны быть расположены
и как их расположение зависит друг от друга
'''

# словарь для пар "abstract_object": [coordinates]

FURN = {}
TECHINFO = {}
PLACED_OBJECTS = {}

coef1 = 2
coef2 = 1.5
COEF = coef1

furn1 = 17
furn2 = 14

FURN_SCALE = furn1

scale_plan2 = FURN_SCALE * COEF
scale_plan1 = FURN_SCALE * COEF
SCALE = scale_plan1

# В этой переменной находятся ВСЕ возможные типы комнат
ROOMS = [
    'bedroom',
    'livingroom',
    'small_bedroom',
    'kitchen',
    'bathroom'
]


#В этой переменной находятся ВСЕ возможные предметы
PRIORITY = {
    'bedroom': [
        'bed', 
        'nightstand', 
        'closet'
        #'carpet', 
        #'flower'
        ],
    'livingroom': [
        'couch',
        'coffee_table',
        'tv'
        #'bookcase',
        #'flower'
        ],
    'small_bedroom': [
        'small_bed', 
        'nightstand', 
        'small_closet'
        #'carpet', 
        #'flower'
        ],
    'kitchen': [
        #'countertop',
        'dining_table'
        #'stove',
        #'sink',
        #'flower'
        ],
    'bathroom': [
        #'bath',
        #'sink',
        #'washer'
    ]
}

DECORATE = {
    'livingroom': 
    {
        'closet': {
            'couch': 'lr',
        }
        #,
#
#        'flower': {
#            'place': 'wall_corner'
#        }
    },
    'bedroom': {}
}

#В этой переменной находятся НЕ ВСЕ возможные предметы, а только те,
# для которых принципиально расположение относительно стены
#functions: wall_center, against_the_wall, in_the_corner, room_center
PLACE = {
    'bed': 'wall_center',
    'bookcase': 'wall_center',
    'nightstand': 'wall_corner',
    'closet': 'wall_center',
    'flower': 'wall_center',
    'couch': 'wall_center',
    'tv': 'special_point',
    'coffee_table': 'wall_center',
    'small_bed': 'wall_center',
    'small_closet': 'wall_corner',
    'dining_table': 'room_center',
    'countertop': 'against_the_wall',
    'stove': 'against_the_wall',
    'sink': 'against_the_wall',
    'bath': 'in_the_corner',
    'washer': 'against_the_wall',
    'carpet': 'wall_center'
}

# В этой переменной находятся НЕ ВСЕ возможные предметы, а только те,
# для которых принципиально расположение относительно других предметов
#functions: near, with_displacement, opposite, in_front_of, in_one_line

DEPENDENCE = {
    'bed': 
    {
        'nightstand': 'near',
        'tv': 'opposite'
        #'carpet': 'with_displacement',
        },
    'couch': 
    {
        'coffee_table': 'in_front_of',
        'tv': 'opposite'
        },
    'small_bed': 
    {
        'small_nightstand': 'near',
        'carpet': 'with_displacement'
    },
    'countertop': 
    {
        'stove': 'in_one_line',
        'sink': 'in_one_line'
    }
}