'''
Обработчик списка стен и его элементов
'''
from typing import List, Dict, Tuple
from _helpers import color
from _geometry import *
"""
room_walls = {'top': [((1505.0, 2499.0), (1673.0, 2499.0)), ((168.0, 3024.0), (595.0, 3024.0))], 'bottom': [((1673.0, 693.0), (350.0, 693.0)), ((343.0, 707.0), (168.0, 707.0)), ((1652.0, 2716.0), (1505.0, 2716.0))], 'left': [((168.0, 707.0), (168.0, 3024.0))], 'right': [((1505.0, 2716.0), (1505.0, 2499.0)), ((1652.0, 3024.0), (1652.0, 2716.0)), ((1673.0, 2499.0), (1673.0, 693.0))]}
"""

def get_size(
    wall: Tuple[Tuple[int], Tuple[int]]
    ):
    dx = abs(wall[0][0]-wall[1][0])
    dy = abs(wall[0][1]-wall[1][1])

    return dx if dx > dy else dy

def find_largest_wall_parameters(
    room_walls: Dict[str, List[Tuple[Tuple[int]]]]
    ):
    max_size = 0
    orient_key = ''
    index = 0

    for orientation in list(room_walls.keys()):
        for i, wall in enumerate(room_walls[orientation]):
            size = get_size(wall)
            if size > max_size:
                max_size = size
                orient_key = orientation
                index = i
    
    return orient_key, index

def recalculate_room_walls(
    room_walls: Dict[str, List[Tuple[Tuple[int]]]], 
    closed_wall: Tuple[Tuple[int]], 
    orient_key: str, 
    bbox: List[List[int]]
    ):
    intervals = new_intervals[orient_key](closed_wall, bbox)
    room_walls[orient_key] = room_walls[orient_key].remove(closed_wall)

    if not room_walls[orient_key]:
        room_walls[orient_key] = []

    print(color.PURPLE + 'Происходит пересчет стен:\n' + str(room_walls) + '\n' + str(intervals) + color.END)

    for interval in intervals:
        room_walls[orient_key].append(interval)

    return room_walls

def horizontal_intersection(
    source_wall: Tuple[Tuple[int]], 
    dest_wall: Tuple[Tuple[int]]
    ):
    if (
        source_wall[0][0] < dest_wall[0][0] 
        and 
        source_wall[1][0] > dest_wall[1][0]
        ):

        return dest_wall

    elif (
        source_wall[0][0] > dest_wall[0][0] 
        and 
        source_wall[1][0] < dest_wall[1][0]
        ):
        return dest_wall

    return None

def vertical_intersection(
    source_wall: Tuple[Tuple[int]], 
    dest_wall: Tuple[Tuple[int]]
    ):
    if (
        source_wall[0][1] < dest_wall[0][1] 
        and source_wall[1][1] > dest_wall[1][1]
        ):

        return dest_wall

    elif (
        source_wall[0][1] > dest_wall[0][1] 
        and 
        source_wall[1][1] < dest_wall[1][1]
        ):

        return dest_wall

    return None

def get_opposite_walls(
    room_walls: Dict[str, List[Tuple[Tuple[int]]]], 
    closed_wall: Tuple[Tuple[int]], 
    orient_key: str
    ):
    opposite_orient_key = get_opposite_orient_key[orient_key]
    dest_walls = room_walls[opposite_orient_key]
    get_intersection = intercestion_functions[orient_key]

    suits_walls = []
    for wall in dest_walls:
        intersection = get_intersection(closed_wall, wall)
        if intersection:
            suits_walls.append(intersection)
    
    availiable_walls = {opposite_orient_key: suits_walls}

    print(color.YELLOW + 'Найдена противоположная стена: ' + str(availiable_walls) + color.END)

    return availiable_walls

def get_tangent(
    source_wall: Tuple[Tuple[int]], 
    dest_wall: Tuple[Tuple[int]]
    ):
    print(color.YELLOW + 'Поиск касания между стенами: ' + str(source_wall) + '   ' + str(dest_wall) + color.END)
    if source_wall[0] == dest_wall[1]:
        #max
        return dest_wall
    elif source_wall[0] == dest_wall[0]:
        #max
        return dest_wall
    elif source_wall[1] == dest_wall[0]:
        #min
        return dest_wall
    elif source_wall[1] == dest_wall[1]:
        #min
        return dest_wall
        
    return None

def get_nearest_walls(
    room_walls: Dict[str, List[Tuple[Tuple[int]]]], 
    closed_wall: Tuple[Tuple[int]], 
    orient_key: str
    ):
    print(room_walls)
    dest_walls = room_walls[orient_key]

    suits_walls = []
    for wall in dest_walls:
        tangent = get_tangent(closed_wall, wall)
        if tangent:
            suits_walls.append(tangent)
    
    availiable_walls = {orient_key: suits_walls}

    print(color.YELLOW + 'Поиск ближайших стен вернул вот это: ' + str(availiable_walls) + color.END)
    
    return availiable_walls

def get_object_params(abstract_object, orient_key):
    concrete_parent = config.FURN[abstract_object]
    if orient_key == 'left' or orient_key == 'right':
        size = max(concrete_parent[0]) - min(concrete_parent[0])
    if orient_key == 'top' or orient_key == 'bottom':
        size = max(concrete_parent[1]) - min(concrete_parent[1])
    
    return size

def get_bias_function(orient_key, size, addiction=20):
    size = size + addiction
    if orient_key == 'bottom':
        bias_function = lambda x1, x2, y1, y2: ((x1, y1 + size), (x2, y2 + size))
    elif orient_key == 'top':
        bias_function = lambda x1, x2, y1, y2: ((x1, y1 - size), (x2, y2 - size))
    elif orient_key == 'left':
        bias_function = lambda x1, x2, y1, y2: ((x1 + size, y1), (x2 + size, y2))
    elif orient_key == 'right':
        bias_function = lambda x1, x2, y1, y2: ((x1 - size, y1), (x2 - size, y2))

    return bias_function

def get_in_front_walls(
    room_walls: Dict[str, List[Tuple[Tuple[int]]]], 
    abstract_object: str,
    closed_wall: Tuple[Tuple[int]], 
    orient_key: str
    ):
    size = get_object_params(abstract_object, orient_key)
    bias_function = get_bias_function(orient_key, size)
    xy1, xy2 = closed_wall
    x1, y1 = xy1
    x2, y2 = xy2

    suits_walls = bias_function(x1, x2, y1, y2)
    availiable_walls = {orient_key: [suits_walls]}

    return availiable_walls


def get_availiable_walls(
    room_walls: Dict[str, List[Tuple[Tuple[int]]]], 
    closed_wall: Tuple[Tuple[int]], 
    orient_key: str, 
    abstract_object: str,
    dependency_type: str
    ):
    """
    Возвращает список стен, доступных для расстановки мебели с учетом
    типа зависимости

    Arguments:
        room_walls {Dict[str, List[Tuple[Tuple[int]]]]} -- [коллекция всех стен]
        closed_wall {Tuple[Tuple[int]]} -- [стена, занятая родительским объектом]
        orient_key {str} -- [ключ ориентации стены, занятой родительским объектом]
        abstract_object {str} -- [абстрактный тип родительского объекта]
        dependency_type {str} -- [тип зависимости между родительским и дочерним объектами]
    
    Returns:
        [type] -- [description]
    """
    if dependency_type == 'near':
        availiable_walls = get_nearest_walls(room_walls, closed_wall, orient_key)

    elif dependency_type == 'opposite':
        availiable_walls = get_opposite_walls(room_walls, closed_wall, orient_key)

    elif dependency_type == 'in_front_of':
        availiable_walls = get_in_front_walls(room_walls, abstract_object, closed_wall, orient_key)

    elif dependency_type == 'both_sides':
        availiable_walls = get_nearest_walls(room_walls, closed_wall, orient_key)
    else:
        return []

    return availiable_walls

def horizontal_proection(
    closed_wall: Tuple[Tuple[int]], 
    bbox: List[List[int]]
    ):
    x_min = min(bbox[0])
    x_max = max(bbox[0])
    y_min = min(closed_wall[0][1], closed_wall[1][1])
    y_max = max(closed_wall[0][1], closed_wall[1][1])

    closed_wall = ((x_min, y_min), (x_max, y_max))

    return closed_wall

def vertical_proection(
    closed_wall: Tuple[Tuple[int]], 
    bbox: List[List[int]]
    ):
    y_min = min(bbox[1])
    y_max = max(bbox[1])
    x_min = min(closed_wall[0][0], closed_wall[1][0])
    x_max = max(closed_wall[0][0], closed_wall[1][0])

    closed_wall = ((x_min, y_min), (x_max, y_max))

    return closed_wall

def bbox_proection(closed_wall, orient_key, bbox):
    print(color.YELLOW + 'Коробка проецируется на прямую: \nbbox: ' + str(bbox) +'\nclosed_wall:' + str(closed_wall) + color.END)
    bbox_proection = proection_functions[orient_key](closed_wall, bbox)
    print(color.YELLOW + 'bbox_proection:  ' + str(bbox_proection) + color.END)

    return bbox_proection

def length_helper():
    horizontal = lambda wall: abs(wall[0][0]-wall[1][0])
    vertical = lambda wall: abs(wall[0][1]-wall[1][1])

    length = {
    'top': horizontal,
    'bottom': horizontal,
    'left': vertical,
    'right': vertical
    }

    return length


get_opposite_orient_key = {
    'top': 'bottom',
    'bottom': 'top',
    'left': 'right',
    'right': 'left'
}

intercestion_functions = {
    'top': horizontal_intersection,
    'bottom': horizontal_intersection,
    'left': vertical_intersection,
    'right': vertical_intersection,
}

proection_functions = {
    'top': horizontal_proection,
    'bottom': horizontal_proection,
    'left': vertical_proection,
    'right': vertical_proection,
}