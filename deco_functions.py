'''
Функции выбора координат объекта на стене и функции определения положения зависимого объекта
'''
#TODO исправить ошибки в знаках
import geometry as gmt
import utils as u
from helpers import color

def near(
    room_walls, 
    orient_key, 
    closed_wall, # ((134.0, 210.0), (194.0, 210.0))
    side='left'
    ):
    '''
    Определяет координаты отрезка стены и точку, в которой 
    зависимый объект должен соприкасаться с родительским.
    '''
    free_walls = room_walls[orient_key]

    if orient_key == 'top' or orient_key == 'bottom':
        a, b, level = gmt.horisontal_line_diapazone(closed_wall)
    else:
        a, b, level = gmt.vertical_line_diapazone(closed_wall)

    #if side in ['any', 'left', 'right', 'both']: #TODO variaty
    if side == 'left':
        if orient_key == 'left' or orient_key == 'right':
            print(color.RED + 'Ошибка функции near, нельзя расположить объект слева от вертикальной стены' + color.END)
            return None, None
        else:
            target_wall = gmt.get_line_with_point(free_walls, a, level)
            side = 'right'

    elif side == 'right':
        if orient_key == 'left' or orient_key == 'right':
            print(color.RED + 'Ошибка функции near, нельзя расположить объект слева от вертикальной стены' + color.END)
            return None, None
        else:
            target_wall = gmt.get_line_with_point(free_walls, b, level)
            side = 'left'

    return target_wall, side

def opposite(
    room_walls,
    orient_key,
    closed_wall,
    ):
    '''
    '''
    if orient_key == 'top':
        orient_key = 'bottom'
    elif orient_key == 'bottom':
        orient_key = 'top'
    elif orient_key == 'left':        
        orient_key = 'right'
    elif orient_key == 'right':
        orient_key = 'left'

    opposite_walls = room_walls[orient_key]

    # searching for intersection 
    # TODO разделить на две функции поиска 
    # горизонтальных и вертикальных пересечений
    if orient_key == 'top' or orient_key == 'bottom':
        a, b = closed_wall[0][0], closed_wall[1][0]
        for wall in opposite_walls:
            if (
                min(a, b) >= min(wall[0][0], wall[1][0]) 
                and 
                max(a, b) <= max(wall[0][0], wall[1][0])
                ):
                print(color.GREEN + '\tУспешно найдена противоположная стена' + color.END)

                return wall, orient_key
        else:
            print(color.RED + '\tПротивоположных стен не найдено' + color.END)

            return None, None
    else:
        a, b = closed_wall[0][1], closed_wall[1][1]
        for wall in opposite_walls:
            if (
                min(a, b) >= min(wall[0][1], wall[1][1]) 
                and 
                max(a, b) <= max(wall[0][1], wall[1][1])
                ):
                print(color.GREEN + '\tУспешно найдена противоположная стена' + color.END)

                return wall, orient_key
        else:
            print(color.RED + '\tПротивоположных стен не найдено' + color.END)

            return None, None

def with_displacement():
    pass

def wall_center(place, orientation, w, h):
    """
    Координаты центра проекции объекта на середину стены
    если 
    place = ((0, 0), (100, 0)), w = 50, h = 50,
    то 
    (x, y) = (50, 0) // если проекция на горизонтальную 
    (x, y) = (0, 50) // если на вертикальную
    """
    # place = ((481.0, 302.0), (511.0, 302.0))
    # orientation = top
    print(color.YELLOW + '\tВыбрано расположение wall_center' + color.END)
    if (orientation == 'top') or (orientation == 'bottom'):
        x = min(place[0][0], place[1][0]) + abs(place[0][0] - place[1][0])/2
        y = place[0][1]
    else:
        x = place[0][0]
        y = min(place[0][1], place[1][1]) + abs(place[0][1] - place[1][1])/2

    return x, y

def wall_corner(place, orientation, w, h, corner='left'):
    """
    Координаты середины проекции объекта на "угол" стены
    если 
    place = ((0, 0), (100, 0)), w = 50, h = 50,
    то 
    (x, y) = (25, 0) // если проекция на вертикальную 
    (x, y) = (0, 25) // если на горизонтальную
    """
    # place = ((481.0, 302.0), (511.0, 302.0))
    # orientation = top
    print(color.YELLOW + '\tВыбрано расположение wall_corner' + color.END)
    if (orientation == 'top') or (orientation == 'bottom'):
        if corner == 'left':
            x = place[0][0] + w/2
            y = place[0][1]
        else:
            x = place[1][0] - w/2
            y = place[1][1]
    else:
        if corner == 'left':
            x = place[0][0]
            y = place[0][1] + h/2
        else:
            x = place[1][0]
            y = place[1][1] - h/2
    print(color.YELLOW + '\tФункция wall corner: place = ' + str(place) + '\n\t' + corner + ' corner ---> (x, y) = (' + str(x) + ' , ' + str(y) + ')' + color.END) #TODO ПРАВЫЙ УГОЛ
    #center_point = object_center_point(orientation, x, y, w, h)

    return x, y

relations_dict = {
    'near': near,
    'opposite': opposite,
    'with_displacement': with_displacement
}

#TODO остальные general_placement_functions
general_placement_functions = {
    'wall_center': wall_center,
    'wall_corner': wall_corner
}