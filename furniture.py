import os
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from visual_result import LoadJson, QLoadAll
import config
path = 'furniture'

class Furniture(object):
    def __init__(self, filename, ttype, width, height, style):
        self.filename = filename
        self.ttype = ttype
        self.width = float(width)
        self.height = float(height)
        self.style = style

    def Area(self):
        return self.width * self.height

    def LoadImage(self, path):
        self.image_name = self.filename[:-5]
        full_path = path + '/' + self.image_name + '.jpg'
        self.image = Image.open(full_path)

    def ImageShow(self):
        self.image.show()


class FurnitureList(list):
    def GetFirst(self):
        if self[0]:
            return self[0]
        else:
            raise IndexError('list index out of range')

    def FilterByType(self, value):
        result = FurnitureList(
            filter(
                lambda x: x.ttype == value, self
            )
        )
        return result

    def FilterByStyle(self, value):
        result = FurnitureList(
            filter(
                lambda x: x.style == value, self
            )
        )
        return result

    def FilterBySize(self, area):
        result = FurnitureList(
            filter(
                lambda x: x.Area() > area, self
            )
        )
        return result

    def LoadImages(self, path):
        for i in self:
            i.LoadImage(path)

    def Scale(self):
        for i in self:
            i.width = i.width * config.FURN_SCALE
            i.height = i.height * config.FURN_SCALE

        return self


def LoadFurniture(
    path=path,
    ):
    ObjsInfo = QLoadAll(path)
    key = lambda a: list(a.keys())[0]

    StyleSortedObjs = (
        [
            Furniture(key(x), **x[key(x)])
            for x
            in ObjsInfo
            if x[key(x)]['style'] != 'ugly'
        ])

    return StyleSortedObjs

# Check
#Mebel = LoadFurniture(path)
#MebelSet = FurnitureList(Mebel)

#OnlyBeds = MebelSet.FilterByType(value='bed')
#OnlyBeds.LoadImages('imgs')

