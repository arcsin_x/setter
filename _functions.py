import config

def kwargs_handler(**kwargs):
    result = (
        kwargs['closed_wall'], kwargs['orient_key'], 
        kwargs['x_y'], kwargs['bbox']
    )

    return result

def set_none(n):
    result = (
        None * n
    )

    return result

def check_place_founded(closed_wall):
    return True if closed_wall else False

def save_results(abstract_object, concrete_obj, result):
    closed_wall, orient_key, x_y, bbox = result
    config.FURN[abstract_object] = bbox
    config.TECHINFO[abstract_object] = {
        'bbox': bbox, 'x_y': x_y, 'orient_key': orient_key, 
        'closed_wall': closed_wall, 'obj': concrete_obj
    }
