'''
Получает bbox объекта в месте, определенном переданными аргументами
'''
import matplotlib.pyplot as plt
from PIL import Image
from _geometry import *

def decorators_generator(*d_args, **d_kwargs):
    '''
    Выбирает декоратор в зависимости от параметрa orientation
    Не учитывает зависимости.
    wall, orientation, object, place
    '''
    wall = d_kwargs['wall']
    obj = d_kwargs['obj']
    abs_obj = d_kwargs['abs_obj']

    if d_kwargs['place']:
        f_place = placement_functions[d_kwargs['place']]
        f_center = object_center_functions[d_kwargs['place']][d_kwargs['orientation']]

    if d_kwargs['orientation'] == 'top':
        def top_decorator(func):
            '''
            wall: ((2639.0, 2086.0), (3178.0, 2086.0))
            '''
            da, db = wall[0][0], wall[1][0]
            const = wall[0][1]
            val = f_place(da, db, pos=d_kwargs['corner_side'], abs_obj=abs_obj)

            x, y = f_center(val, const, obj, pos=d_kwargs['corner_side'], abs_obj=abs_obj)
            bbox = bbox_coordinates((x, y), obj.width, obj.height)

            plt.scatter(val, const)
            plt.scatter(x, y)

            def wrapped(*f_args, **f_kwargs): 
                plt.plot((wall[0][0], wall[1][0]), (wall[0][1], wall[1][1]))
                plt.plot(bbox[0], bbox[1])
                # здесь можно сделать всякие проверки и по итогу добавлять 
                # bbox, wall etc в аргументы функции
                # проверка пересечения
                f_kwargs['orient_key'] = d_kwargs['orientation']
                f_kwargs['closed_wall'] = wall
                f_kwargs['x_y'] = (x, y)
                f_kwargs['bbox'] = bbox

                #obj.new_image = obj.image

                return func(*f_args, **f_kwargs)
            return wrapped
        return top_decorator

    elif d_kwargs['orientation'] == 'left':
        def left_decorator(func):
            '''
            wall = ((2639.0, 1694.0), (2639.0, 2086.0))
            '''
            da, db = wall[0][1], wall[1][1]
            const = wall[0][0]
            val = f_place(da, db, pos=d_kwargs['corner_side'], abs_obj=abs_obj)

            x, y = f_center(val, const, obj, pos=d_kwargs['corner_side'], abs_obj=abs_obj)
            bbox = bbox_coordinates((x, y), obj.height, obj.width) #объект должен быть повернут

            plt.scatter(const, val)
            def wrapped(*f_args, **f_kwargs):
                plt.plot((wall[0][0], wall[1][0]), (wall[0][1], wall[1][1]))
                plt.plot(bbox[0], bbox[1])

                f_kwargs['orient_key'] = d_kwargs['orientation']
                f_kwargs['closed_wall'] = wall
                f_kwargs['x_y'] = (x, y)
                f_kwargs['bbox'] = bbox

                #obj.new_image = obj.image.transpose(Image.ROTATE_90)

                return func(*f_args, **f_kwargs)
            return wrapped
        return left_decorator

    elif d_kwargs['orientation'] == 'bottom':
        def bottom_decorator(func):
            '''
            wall = 
            '''
            da, db = wall[0][0], wall[1][0]
            const = wall[0][1]
            val = f_place(da, db, pos=d_kwargs['corner_side'], abs_obj=abs_obj)

            x, y = f_center(val, const, obj, pos=d_kwargs['corner_side'], abs_obj=abs_obj)
            bbox = bbox_coordinates((x, y), obj.width, obj.height)

            plt.scatter(val, const)
            def wrapped(*f_args, **f_kwargs):
                plt.plot((wall[0][0], wall[1][0]), (wall[0][1], wall[1][1]))
                plt.plot(bbox[0], bbox[1])

                f_kwargs['orient_key'] = d_kwargs['orientation']
                f_kwargs['closed_wall'] = wall
                f_kwargs['x_y'] = (x, y)
                f_kwargs['bbox'] = bbox

                #obj.new_image = obj.image.transpose(Image.ROTATE_180)

                return func(*f_args, **f_kwargs)
            return wrapped
        return bottom_decorator

    elif d_kwargs['orientation'] == 'right':
        def right_decorator(func):
            '''
            wall =
            '''
            da, db = wall[0][1], wall[1][1]
            const = wall[0][0]
            val = f_place(da, db, pos=d_kwargs['corner_side'], abs_obj=abs_obj)

            x, y = f_center(val, const, obj, pos=d_kwargs['corner_side'], abs_obj=abs_obj)
            bbox = bbox_coordinates((x, y), obj.height, obj.width) #объект должен быть повернут

            plt.scatter(const, val)
            def wrapped(*f_args, **f_kwargs):
                plt.plot((wall[0][0], wall[1][0]), (wall[0][1], wall[1][1]))
                plt.plot(bbox[0], bbox[1])

                f_kwargs['orient_key'] = d_kwargs['orientation']
                f_kwargs['closed_wall'] = wall #TODO тогда, когда объект прошел все проверочки
                f_kwargs['x_y'] = (x, y)
                f_kwargs['bbox'] = bbox

               # obj.new_image = obj.image.transpose(Image.ROTATE_270)

                return func(*f_args, **f_kwargs)
            return wrapped
        return right_decorator


# Пример исользования функции get_coordinates с декоратором.
"""
@decorators_generator(
                    orientation=orientation, #зависит от типа стены
                    obj=obj,
                    wall=wall,
                    place='wall_center', #зависит от типа объекта
                    corner_side='min'
                    )

def get_coordinates(*args, **kwargs):
    if kwargs['closed_wall']:
        print(kwargs['closed_wall'])
        print(kwargs['orient_key'])
        print(kwargs['bbox'])

        return kwargs['closed_wall'], kwargs['orient_key']
    else:

        return None, None

a, b = get_coordinates(
    walls=walls, bbox=bbox_collect, 
    orient_key=orient_key, closed_wall=None
    )
plt.show()
"""