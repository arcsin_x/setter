'''
Функции для определения основной геометрии
'''
import math
import config
from _helpers import color

def bbox_coordinates(
    center_point, 
    w, h
    ):
    '''
    Вернуть список с координатами вида 
    [[x1, x2, ..., xn], [y1, y2, ..., yn]]
    '''
    #w, h = u.get_main_side(orientation, w, h)
    x_center, y_center = center_point

    x_a = x_center - w/2
    y_a = y_center - h/2

    x_b = x_center + w/2
    y_b = y_center - h/2

    x_c = x_center + w/2
    y_c = y_center + h/2

    x_d = x_center - w/2
    y_d = y_center + h/2

    coordinates = [[x_a, x_b, x_c, x_d, x_a], [y_a, y_b, y_c, y_d, y_a]]

    print(color.YELLOW + '\tФункция bbox_coordinates: ' + str(coordinates) + color.END)

    return coordinates

def get_line_with_point(free_walls, a, b):
    """
    Вернуть стену, отрезок которой содержит точку (a, b)
    """
    for wall in free_walls:
        if wall[0] == (a, b) or wall[1] == (a, b):
            target_wall = wall
            break
    else:
        return None
        print(color.RED + 'Ошибка функции get_line_with_point\nfree_walls : ' + str(free_walls) + '\n(x, y) : (' + str(a) + ', ' + str(b) + ')' + color.END)

    return target_wall

def new_intervals_horizontal( 
    wall, 
    coordinates
    ):
    """
    Вырезает из исходного отрезка, описывающего стену
    проекцию bbox'а только что размещенного у этой стены объекта
    """
    # получаем параметры x исходной стены
    x0_left, x0_right, y = horisontal_line_diapazone(wall)

    # получаем x #TODO проекции на стену для объекта
    x_list, y_list = coordinates
    x1_left, x1_right = min(x_list), max(x_list)

    # пересчитываем новые интервалы
    intervals = []
    new_interval = lambda x1, x2, y: ((x1, y), (x2, y))

    if x0_left != x1_left:
        intervals.append(new_interval(x0_left, x1_left, y))
    if x1_right != x0_right:
        intervals.append(new_interval(x1_right, x0_right, y))

    #closed_interval = new_interval(x1_left, x1_right, y)

    return intervals#, closed_interval

def new_intervals_vertical( 
    wall, 
    coordinates
    ):
    """
    """
    # получаем параметры x исходной стены
    y0_bot, y0_top, x = vertical_line_diapazone(wall)

    # получаем x #TODO проекции на стену для объекта
    x_list, y_list = coordinates
    y1_bot, y1_top = min(y_list), max(y_list)
    print(color.RED + str(y1_bot) + ' ' + str(y1_top) + color.END)

    # пересчитываем новые интервалы
    intervals = []
    new_interval = lambda x, y1, y2: ((x, y1), (x, y2))

    if y0_bot != y1_bot:
        intervals.append(new_interval(x, y0_bot, y1_bot))
    if y1_bot != y0_top:
        intervals.append(new_interval(x, y1_top, y0_top))

    return intervals

def horisontal_line_diapazone(wall):
    '''
    Возвращает x_min и x_max для горизонтальной стены
    '''
    print(wall)
    x = [coordinate[0] for coordinate in wall]
    y = wall[0][1]

    x_left, x_right = min(x), max(x)

    return x_left, x_right, y

def vertical_line_diapazone(wall):
    '''
    Возвращает y_min и y_max для вертикальной стены
    '''
    y = [coordinate[1] for coordinate in wall]
    x = wall[1][0]

    y_bot, y_top = min(y), max(y)

    return y_bot, y_top, x


def wall_center(da, db, pos='min', abs_obj=None):
    return (db+da)/2

def wall_corner(da, db, pos='min', abs_obj=None):
    if pos == 'min':
        return da 
    else:
        return db

def special_point(da, db, pos='min', abs_obj=None):
    '''
    по const подставляет точку из родительского объекта
    '''
    if abs_obj == None:
        print('special point error')
        print(abs_obj)
    point = config.TECHINFO[abs_obj]['x_y'][1] #TODO left-right

    return point

def get_object_params(obj):
    return obj.width, obj.height

def object_point_center_top(val, const, obj, pos='min', abs_obj=None):
    w, h = get_object_params(obj)
    x_center = val
    y_center = const - h/2
    return x_center, y_center

def object_point_center_bottom(val, const, obj, pos='min', abs_obj=None):
    w, h = get_object_params(obj)
    x_center = val
    y_center = const + h/2
    return x_center, y_center

def object_point_center_left(val, const, obj, pos='min', abs_obj=None):
    w, h = get_object_params(obj)
    x_center = const + h/2
    y_center = val
    return x_center, y_center

def object_point_center_right(val, const, obj, pos='min', abs_obj=None):
    w, h = get_object_params(obj)
    x_center = const - h/2
    y_center = val
    return x_center, y_center

def object_point_corner_top(val, const, obj, pos='min', abs_obj=None):
    w, h = get_object_params(obj)
    if pos == 'min':
        x_center = val + w/2
        y_center = const - h/2
    elif pos == 'max':
        x_center = val - w/2
        y_center = const - h/2
    return x_center, y_center

def object_point_corner_bottom(val, const, obj, pos='min', abs_obj=None):
    w, h = get_object_params(obj)
    if pos == 'min':
        x_center = val + w/2
        y_center = const + h/2
    elif pos == 'max':
        x_center = val - w/2
        y_center = const + h/2
    return x_center, y_center

def object_point_corner_left(val, const, obj, pos='min', abs_obj=None):
    w, h = get_object_params(obj)
    if pos == 'min':
        x_center = const + h/2
        y_center = val + w/2
    elif pos == 'max':
        x_center = const + h/2
        y_center = val - w/2
    return x_center, y_center

def object_point_corner_right(val, const, obj, pos = 'min', abs_obj=None):
    w, h = get_object_params(obj)
    if pos == 'min':
        x_center = const - h/2
        y_center = val + w/2
    elif pos == 'max':
        x_center = const - h/2
        y_center = val - w/2
    return x_center, y_center

def object_point_special_top(val, const, obj, pos = 'min', abs_obj=None):
    w, h = get_object_params(obj)

    x_center = val
    y_center = const - h/2
    return x_center, y_center


def euclidean_distance(
    a, b # vectors
    ):
    '''
    Евклидово расстояние между двумя векторами произвольной,
    но равной размерности 
    type(a), type(b) == iterable
    '''
    assert len(a)==len(b);

    summa = 0
    for i in range(len(a)):
        summa += (abs(a[i] - b[i]))**2

    return math.sqrt(summa)

new_intervals = {
    'top': new_intervals_horizontal,
    'bottom': new_intervals_horizontal,
    'left': new_intervals_vertical,
    'right': new_intervals_vertical
}

placement_functions = {
    'wall_center': wall_center,
    'wall_corner': wall_corner,
    'special_point': special_point
}

object_center_functions = {
    'wall_center': {
        'top': object_point_center_top,
        'bottom': object_point_center_bottom,
        'left': object_point_center_left,
        'right': object_point_center_right
    },
    'wall_corner': {
        'top': object_point_corner_top,
        'bottom': object_point_corner_bottom,
        'left': object_point_corner_left,
        'right': object_point_corner_right
    },
    'special_point': {
        'top': object_point_center_top,
        'bottom': object_point_center_bottom,
        'left': object_point_center_left,
        'right': object_point_center_right
    }
}