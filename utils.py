'''
Вспомогательные функции
'''
import config
import geometry as gmt

from helpers import color
import deco_functions as df
import geometry as gmt

def get_priority(room_type, object_type):
    return config.PRIORITY[room_type].index(object_type)

def get_main_side(orient, w, h):
    if orient == 'top' or orient == 'bottom':
        return w, h
    else:
        return h, w

def standart_nested_loop(list1, list2):
    for a in list1:
        for b in list2:
            yield a, b

def nested_loop(concrete_objects, room_walls):
    for concrete_obj in concrete_objects:
        for orient_key in list(room_walls.keys()):
            for wall in room_walls[orient_key]:
                yield concrete_obj, orient_key, wall

def bbox_to_vector(
    bbox
    ):
    x, y = bbox
    assert len(x) == len(y);

    vector = []

    for i in range(len(x)):
        vector.append([x[i], y[i]])

    return vector

def change_walls(
    room_walls, # {'top': [((228.0, 158.0), (322.0, 158.0))], 'bottom': [...], 'left': [...], 'right': [...]}
    orient_key, # top
    wall, # ((228.0, 158.0), (322.0, 158.0))
    coordinates # [[232.0, 322.0, 322.0, 232.0, 232.0], [90.5, 90.5, 290.5, 290.5, 90.5]]
    ):
    '''
    Обновляет словарь координат, вырезая отрезок из стены, на которой был расположен объект
    '''
    oriented_list = room_walls[orient_key] #получить список по ключу ориентации
    index = oriented_list.index(wall) #получить индекс выбранной стены

    new_walls, closed_wall = gmt.new_intervals_on_top(wall, coordinates)
    room_walls.update({orient_key: new_walls})

    print(color.YELLOW + '\t\tСтены успешно пересчитаны' + color.END)

    return room_walls, closed_wall

def place_object(wall, orientation, concrete_object, corner='right', gen_place='wall_center'):
    '''
    Подбирает координаты бокса объекта около стены
    '''
    if orientation == 'top' or orientation == 'bottom':
        w, h = concrete_object.width, concrete_object.height
    else:
        h, w = concrete_object.width, concrete_object.height

    if gen_place == 'wall_corner':
        x, y = df.general_placement_functions[gen_place](wall, orientation, w, h, corner)
    else:
        #TODO костыль, который нужно убрать
        if not gen_place in list(df.general_placement_functions.keys()):
            x, y = df.general_placement_functions['wall_center'](wall, orientation, w, h)
        else:
            x, y = df.general_placement_functions[gen_place](wall, orientation, w, h)

    center_point = gmt.object_center_point(orientation, x, y, w, h)
    coordinates = gmt.bbox_coordinates(center_point, w, h)

    print(color.YELLOW + '\tКоординаты положения объекта успешно подобраны' + color.END)

    return coordinates