'''
Получает bbox объекта в месте, определенном переданными аргументами
'''
import matplotlib.pyplot as plt
from geometry import bbox_coordinates, new_intervals, horisontal_line_diapazone, vertical_line_diapazone

#orient_key = ''
#orientation = 'left'
#place = 'wall_center'
#obj = MebelSet[7]

def wall_center(da, db, pos='min'):
    return (db+da)/2

def wall_corner(da, db, pos='min'):
    if pos == 'min':
        return da 
    else:
        return db

def get_object_params(obj):
    return obj.width, obj.height

def object_point_center_top(val, const, obj, pos='min'):
    w, h = get_object_params(obj)
    x_center = val
    y_center = const - h/2
    return x_center, y_center

def object_point_center_bottom(val, const, obj, pos='min'):
    w, h = get_object_params(obj)
    x_center = val
    y_center = const + h/2
    return x_center, y_center

def object_point_center_left(val, const, obj, pos='min'):
    w, h = get_object_params(obj)
    x_center = const + h/2
    y_center = val
    return x_center, y_center

def object_point_center_right(val, const, obj, pos='min'):
    w, h = get_object_params(obj)
    x_center = const - h/2
    y_center = val
    return x_center, y_center

def object_point_corner_top(val, const, obj, pos='min'):
    w, h = get_object_params(obj)
    if pos == 'min':
        x_center = val + w/2
        y_center = const - h/2
    elif pos == 'max':
        x_center = val - w/2
        y_center = const - h/2
    return x_center, y_center

def object_point_corner_bottom(val, const, obj, pos='min'):
    w, h = get_object_params(obj)
    if pos == 'min':
        x_center = val + w/2
        y_center = const + h/2
    elif pos == 'max':
        x_center = val - w/2
        y_center = const + h/2
    return x_center, y_center

def object_point_corner_left(val, const, obj, pos='min'):
    w, h = get_object_params(obj)
    if pos == 'min':
        x_center = const + h/2
        y_center = val + w/2
    elif pos == 'max':
        x_center = const + h/2
        y_center = val - w/2
    return x_center, y_center

def object_point_corner_right(val, const, obj, pos = 'min'):
    w, h = get_object_params(obj)
    if pos == 'min':
        x_center = const - h/2
        y_center = val + w/2
    elif pos == 'max':
        x_center = const - h/2
        y_center = val - w/2
    return x_center, y_center


placement_functions = {
    'wall_center': wall_center,
    'wall_corner': wall_corner
}

object_center_functions = {
    'wall_center': {
        'top': object_point_center_top,
        'bottom': object_point_center_bottom,
        'left': object_point_center_left,
        'right': object_point_center_right
    },
    'wall_corner': {
        'top': object_point_corner_top,
        'bottom': object_point_corner_bottom,
        'left': object_point_corner_left,
        'right': object_point_corner_right
    }    
}


def decorators_generator(*d_args, **d_kwargs):
    '''
    Выбирает декоратор в зависимости от параметрa orientation
    Не учитывает зависимости.
    wall, orientation, object, place
    '''
    wall = d_kwargs['wall']
    obj = d_kwargs['obj']

    if d_kwargs['place']:
        f_place = placement_functions[d_kwargs['place']]
        f_center = object_center_functions[d_kwargs['place']][d_kwargs['orientation']]

    if d_kwargs['orientation'] == 'top':
        def top_decorator(func):
            '''
            wall: ((2639.0, 2086.0), (3178.0, 2086.0))
            '''
            da, db = wall[0][0], wall[1][0]
            const = wall[0][1]
            val = f_place(da, db, pos=d_kwargs['corner_side'])

            x, y = f_center(val, const, obj, pos=d_kwargs['corner_side'])
            bbox = bbox_coordinates((x, y), obj.width, obj.height)

            plt.scatter(val, const)
            plt.scatter(x, y)

            def wrapped(*f_args, **f_kwargs): 
                plt.plot((wall[0][0], wall[1][0]), (wall[0][1], wall[1][1]))
                plt.plot(bbox[0], bbox[1])
                # здесь можно сделать всякие проверки и по итогу добавлять 
                # bbox, wall etc в аргументы функции
                # проверка пересечения

                f_kwargs['orient_key'] = d_kwargs['orientation']
                f_kwargs['closed_wall'] = wall
                f_kwargs['bbox'] = bbox

                return func(*f_args, **f_kwargs)
            return wrapped
        return top_decorator

    elif d_kwargs['orientation'] == 'left':
        def left_decorator(func):
            '''
            wall = ((2639.0, 1694.0), (2639.0, 2086.0))
            '''
            da, db = wall[0][1], wall[1][1]
            const = wall[0][0]
            val = f_place(da, db, pos=d_kwargs['corner_side'])

            x, y = f_center(val, const, obj, pos=d_kwargs['corner_side'])
            bbox = bbox_coordinates((x, y), obj.height, obj.width) #объект должен быть повернут

            plt.scatter(const, val)
            def wrapped(*f_args, **f_kwargs):
                plt.plot((wall[0][0], wall[1][0]), (wall[0][1], wall[1][1]))
                plt.plot(bbox[0], bbox[1])

                f_kwargs['orient_key'] = d_kwargs['orientation']
                f_kwargs['closed_wall'] = wall
                f_kwargs['bbox'] = bbox


                return func(*f_args, **f_kwargs)
            return wrapped
        return left_decorator

    elif d_kwargs['orientation'] == 'bottom':
        def bottom_decorator(func):
            '''
            wall = 
            '''
            da, db = wall[0][0], wall[1][0]
            const = wall[0][1]
            val = f_place(da, db, pos=d_kwargs['corner_side'])

            x, y = f_center(val, const, obj, pos=d_kwargs['corner_side'])
            bbox = bbox_coordinates((x, y), obj.width, obj.height)

            plt.scatter(val, const)
            def wrapped(*f_args, **f_kwargs):
                plt.plot((wall[0][0], wall[1][0]), (wall[0][1], wall[1][1]))
                plt.plot(bbox[0], bbox[1])

                f_kwargs['orient_key'] = d_kwargs['orientation']
                f_kwargs['closed_wall'] = wall
                f_kwargs['bbox'] = bbox

                return func(*f_args, **f_kwargs)
            return wrapped
        return bottom_decorator

    elif d_kwargs['orientation'] == 'right':
        def right_decorator(func):
            '''
            wall =
            '''
            da, db = wall[0][1], wall[1][1]
            const = wall[0][0]
            val = f_place(da, db, pos=d_kwargs['corner_side'])

            x, y = f_center(val, const, obj, pos=d_kwargs['corner_side'])
            bbox = bbox_coordinates((x, y), obj.height, obj.width) #объект должен быть повернут

            plt.scatter(const, val)
            def wrapped(*f_args, **f_kwargs):
                plt.plot((wall[0][0], wall[1][0]), (wall[0][1], wall[1][1]))
                plt.plot(bbox[0], bbox[1])

                f_kwargs['orient_key'] = d_kwargs['orientation']
                f_kwargs['closed_wall'] = wall #TODO тогда, когда объект прошел все проверочки
                f_kwargs['bbox'] = bbox

                return func(*f_args, **f_kwargs)
            return wrapped
        return right_decorator

# Пример исользования функции get_coordinates с декоратором.
"""
@decorators_generator(
                    orientation=orientation, #зависит от типа стены
                    obj=obj,
                    wall=wall,
                    place='wall_center', #зависит от типа объекта
                    corner_side='min'
                    )

def get_coordinates(*args, **kwargs):
    if kwargs['closed_wall']:
        print(kwargs['closed_wall'])
        print(kwargs['orient_key'])
        print(kwargs['bbox'])

        return kwargs['closed_wall'], kwargs['orient_key']
    else:

        return None, None

a, b = get_coordinates(
    walls=walls, bbox=bbox_collect, 
    orient_key=orient_key, closed_wall=None
    )
plt.show()
"""



"""
Другая форма записи
def get_coordinates(*args, **kwargs):
    pass

ful_function = decorators_generator(var1=var1, var2=var2)(get_coordinates)
ful_function(1, 2)

all_walls = {
    'top': [((2639.0, 2086.0), (3178.0, 2086.0))], 
    'left': [((2639.0, 1694.0), (2639.0, 2086.0))],
    'bottom': [((3178.0, 1694.0), (2639.0, 1694.0))], 
    'right': [((3178.0, 2086.0), (3178.0, 1694.0))]
    }

orients = list(all_walls.keys())

for orient in orients:
    walls = all_walls[orient]
    for wall in walls:
        @decorators_generator(
        orientation=orient, #зависит от типа стены
        walls=all_walls,
        obj=obj,
        wall=wall,
        place='wall_center', #зависит от типа объекта
        corner_side='min'
        )
        def get_coordinates(*args, **kwargs):
            pass
        print(orient)
        get_coordinates()

plt.show()
"""
"""
bbox_collect = []

length = {
    'top': horizontal,
    'bottom': horizontal,
    'left': vertical,
    'right': vertical
}

horizontal = lambda wall: abs(wall[0][0]-wall[1][0])
vertical = lambda wall: abs(wall[0][1]-wall[1][1])

for i, room in enumerate(test.rooms):
    if i == 4:
        all_walls = {
        'top': room.top, 
        'left': room.left,
        'bottom': room.bottom, 
        'right': room.right
        }
        orients = list(all_walls.keys())

        for orient in orients:
            get_length = length[orient]
            walls = all_walls[orient]
            for wall in walls:
                if get_length(wall) > obj.width:
                    @decorators_generator(
                    orientation=orient, #зависит от типа стены
                    obj=obj,
                    wall=wall,
                    place='wall_center', #зависит от типа объекта
                    corner_side='min'
                    )
                    def get_coordinates(*args, **kwargs):
                        #финальные процедуры с полученными значениями
                        print(kwargs['bbox_collect'])
                        return
                    get_coordinates(walls=walls, bbox_collect=bbox_collect)
    plt.show()
"""