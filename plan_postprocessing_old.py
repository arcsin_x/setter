'''
Постобработка распознанных планировок. ВЕРНУТЬСЯ К ЭТОМУ МОДУЛЮ В FURNITURE_SET
'''
import json
import numpy as np
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import collections as mc

from descartes import PolygonPatch
from shapely.ops import cascaded_union
from itertools import combinations
from shapely.geometry import Point, LineString, Polygon, LinearRing, MultiPolygon
import config


ReLine = lambda a: (
(
    a['x_diapazone'][0], 
    a['y_diapazone'][0]
    ), 
(
    a['x_diapazone'][1], 
    a['y_diapazone'][1]
    )
)

class Plan(object):

    def __init__(self, filepath):
        self.filepath = filepath
        with open(self.filepath) as f:
            j = json.loads(f.read())

        self.data = ({
            'doors': j['doors_contours'],
            'windows': j['window_contours'],
            'walls': j['wall_contours'],
            'room': j['rooms_contours'],
            'types': j['room_types']
        })

        self.doors = self.data['doors']
        self.windows = self.data['windows']
        self.walls = self.data['walls']
        self.room = self.data['room']
        self.ttypes = self.data['types']

        #ЭТО КРИВОЕ ТЕСТОВОЕ МАСШТАБИРОВАНИЕ
        self.room = [[[x*config.SCALE, y*config.SCALE] for (x, y) in room_num] for room_num in self.room]
        self.windows = [[[[x*config.SCALE, y*config.SCALE] for (x, y) in room_num] for room_num in d] for d in self.windows]
        self.walls = [[[[x*config.SCALE, y*config.SCALE] for (x, y) in room_num] for room_num in d] for d in self.walls]
        self.doors = [[[[x*config.SCALE, y*config.SCALE] for (x, y) in room_num] for room_num in d] for d in self.doors]

        self.wal = []
        self.win = []
        self.d = []

        print('Ok')

    def ImposeWindows(self):
        self.win = []
        for window in self.windows:
            win_set = []
            points = sum(window, [])
            for point in points:
                win_set.append(tuple((point[0], point[1])))
            self.win.append(Polygon(LinearRing(win_set)))
        print(len(self.win))
        print("Windows Ok")

    def ImposeDoors(self):
        self.d = []
        for door in self.doors:
            d_set = []
            points = sum(door, [])
            for point in points:
                d_set.append(tuple((point[0], point[1])))
            self.d.append(Polygon(LinearRing(d_set)))

        print("Doors Ok")

    def ImposeWalls(self):
        self.wal = []
        for wall in self.walls:
            wal_set = []
            points = sum(wall, [])
            for point in points:
                wal_set.append(tuple((point[0], point[1])))
            self.wal.append(Polygon(LinearRing(wal_set)))

    def SplitByRooms(self):


        class Room(object):
            def __init__(self, param_dict):
                self.ttype = param_dict['type']
                self.room = param_dict['room']
                #self.walls = sum(param_dict['walls'], [])
                #self.windows = sum(param_dict['windows'], [])
                #self.doors = sum(param_dict['doors'], [])
                self.furniture = []

                self.walls_poly = []
                self.doors_poly = []
                self.windows_poly = []

                self.top = []
                self.bottom = []
                self.left = []
                self.right = []
                
            def Restruct(self):
                self.cnt = []

                self.cnt = CntWriter(self.room)
                self.cnt = Chopping(self.cnt)
                
                return self.cnt

            def MergeDirection(self, coef=1):
                points = self.cnt.shape[0]
                
                self.horisontal_lines = []
                self.vertical_lines = []
                self.diagonal_lines = []

                for i in range(points-1):

                    xDis, yDis = False, False
                    x1, x2 = self.cnt[i][0], self.cnt[i+1][0]
                    y1, y2 = self.cnt[i][1], self.cnt[i+1][1]

                    if abs(x1 - x2) > coef:
                        xDis = True
                    if abs(y1 - y2) > coef:
                        yDis = True

                    line = LineString([Point(x1, y1), Point(x2, y2)])
                    
                    if xDis and yDis:
                        self.diagonal_lines.append(line)
                    elif xDis:
                        self.horisontal_lines.append(line)
                    elif yDis:
                        self.vertical_lines.append(line)
                    else:
                        #TODO Они не удаляются
                        print('Deleting dublicate from coordinates array of Room object')
                        np.delete(self.cnt, i, 0)
                        #print(str(i) + ': ' + str(self.cnt[i]))

            def TB(self):
                '''
                horizontal lines to top/bottom lines
                '''
                self.CreatePolygon()
                
                x_diapazone = []
                y_diapazone = []
                # преобразуем список горизонтальных линий в pd.DataFrame
                lines = self.horisontal_lines
                for i in range(len(lines)):
                    x, y = lines[i].xy
                    x_diapazone.append(list(x))
                    y_diapazone.append(list(y))
                data = {'x_diapazone': x_diapazone, 'y_diapazone': y_diapazone}
                data_df = pd.DataFrame(data)
                # сортируем dataFrame и идем по нему
                sorted_df = data_df.sort_values(by='y_diapazone') #здесь отличие
                for a, b in sorted_df.iterrows():
                    # определяем точку середины горизонт.линии
                    xmin = min(b['x_diapazone'])
                    xmax = max(b['x_diapazone'])
                    x_point = (xmax + xmin)/2
                    # определяем точку по y выше и ниже текущей
                    y_point1 = min(b['y_diapazone']) + 1
                    y_point2 = max(b['y_diapazone']) - 1
                    point1 = Point((x_point, y_point1))
                    point2 = Point((x_point, y_point2))
                    # проверяем, точки на содержание в полигоне
                    res1 = point1.within(self.polygon)   
                    res2 = point2.within(self.polygon) 
                    # отображаем на графике
                    plt.scatter(x_point, y_point1)
                    plt.scatter(x_point, y_point2)
                    # раскидываем по атрибутам класса
                    if res1 == True and res2 == False:
                        self.bottom.append(ReLine(b))
                    elif res1 == False and res2 == True:
                        self.top.append(ReLine(b))
                    else:
                        print('err')
            
            def LR(self):
                '''
                vertical lines to left/right lines
                '''
                self.CreatePolygon()

                x_diapazone = []
                y_diapazone = []

                lines = self.vertical_lines
                for i in range(len(lines)):
                    x, y = lines[i].xy
                    x_diapazone.append(list(x))
                    y_diapazone.append(list(y))
                data = {'x_diapazone': x_diapazone, 'y_diapazone': y_diapazone}
                data_df = pd.DataFrame(data)

                sorted_df = data_df.sort_values(by='x_diapazone')
                for a, b in sorted_df.iterrows():
                    print(b)
                    ymin = min(b['y_diapazone'])
                    ymax = max(b['y_diapazone'])
                    y_point = (ymax + ymin)/2
                    print(y_point)
                    x_point1 = min(b['x_diapazone']) + 1
                    x_point2 = max(b['x_diapazone']) - 1
                    point1 = Point((x_point1, y_point))
                    point2 = Point((x_point2, y_point))
                    print(point1, point2)
                    res1 = point1.within(self.polygon)   
                    res2 = point2.within(self.polygon) 
                    plt.scatter(x_point1, y_point)
                    plt.scatter(x_point2, y_point)

                    if res1 == True and res2 == False:
                        self.left.append(ReLine(b))
                    elif res1 == False and res2 == True:
                        self.right.append(ReLine(b))
                    else:
                        print('err')

            def ImposePolygon(self):
                poly_set = []
                for point in self.room:
                    poly_set.append(tuple((point[0], point[1])))
                self.polygon = Polygon(LinearRing(poly_set))        

            def CreatePolygon(self):
                # TODO падает if
                plt.plot(*self.polygon.exterior.xy)
                if self.centroid_point:
                    plt.scatter(self.centroid_point.x,
                                        self.centroid_point.y)

            def Center(self):
                self.centroid_point = self.polygon.centroid
                self.centroid = self.centroid_point.wkt

            def Show(self):
                plt.show()

            def ImposeData(self, keys=['walls', 'doors', 'windows']):
                self.windows_poly = Polygon(LinearRing(
                                                    [tuple((point[0], point[1])) 
                                                    for point 
                                                    in self.windows])
                                                    )
                self.doors_poly = Polygon(LinearRing(
                                                    [tuple((point[0], point[1])) 
                                                    for point 
                                                    in self.doors])
                                                    )
                self.walls_poly = Polygon(LinearRing(
                                                    [tuple((point[0], point[1])) 
                                                    for point 
                                                    in self.walls])
                                                    )

                print("Everything Ok")

            def Substract(self):
                tt = []
                if self.polygon.intersects(self.win) == True:
                    nonoverlap = (self.polygon.symmetric_difference(self.win)).difference(self.win)
                    tt.append(nonoverlap)
                else:
                    tt.append(self.polygon)

                fin = MultiPolygon(tt)

                return fin

            def CreateLines(self,
                            lines_top=None,
                            lines_bottom=None,
                            lines_left=None,
                            lines_right=None
                            ):
                lines_top += [[p[0], p[1]] for p in self.top]
                lines_bottom += [[p[0], p[1]] for p in self.bottom]
                lines_left += [[p[0], p[1]] for p in self.left]
                lines_right += [[p[0], p[1]] for p in self.right]

                return lines_top, lines_bottom, lines_left, lines_right


        class RoomList(list):
            def Apply(self):
                for room in self:
                    room.Restruct()
                    room.MergeDirection()
                    room.ImposePolygon()
                    room.Center()
                    room.CreatePolygon()
            
            def Show(self):
                plt.show()

            def Print(self):
                for room in self:
                    for line in room.horisontal_lines:
                        print(line.length)
                    for line in room.vertical_lines:
                        print(line.length)
                    for line in room.diagonal_lines:
                        print(line.length)

            def ShowLines(self):
                fig, ax = plt.subplots()

                lines_top=[]
                lines_bottom=[]
                lines_left=[]
                lines_right=[]

                for x in self:
                    lines_top, lines_bottom, lines_left, lines_right = x.CreateLines(
                        lines_top, 
                        lines_bottom, 
                        lines_left, 
                        lines_right
                        )

                lct = mc.LineCollection(lines_top, colors='r', linewidths=2)
                ax.add_collection(lct)
                lcb = mc.LineCollection(lines_bottom, colors='b', linewidths=2)
                ax.add_collection(lcb)
                lcl = mc.LineCollection(lines_left, colors='g', linewidths=2)
                ax.add_collection(lcl)
                lcr = mc.LineCollection(lines_right, colors='yellow', linewidths=2)
                ax.add_collection(lcr)


                #x_test = [88.0, 88.0, 85.0, 85.0, 607.0, 607.0]
                #y_test = [107.0, 135.0, 254.0, 318.0, 109.0, 186.0]
                #ax.scatter(x_test, y_test)

                #x_test_d = [88.0, 88.0, 324.0, 324.0, 350.0, 350.0, 320.0, 266.0, 320.0, 266.0, 240.0, 240.0, 254.0, 254.0, 301.0, 264.0, 301.0, 264.0, 357.0, 357.0]
                #y_test_d = [133.0, 174.0, 296.0, 169.0, 296.0, 169.0, 296.0, 296.0, 318.0, 318.0, 210.0, 173.0, 210.0, 173.0, 173.0, 173.0, 158.0, 158.0, 357.0, 357.0]
                #ax.scatter(x_test_d, y_test_d)

                test.PlanVis()
                ax.autoscale()
                fig.show()

            def PlanVis(self):
                test.ImposeWindows()
                test.ImposeDoors()
                test.ImposeWalls()
                fig = plt.figure()
                ax = fig.gca()
                print(len(test.win))
                for poly in test.win:
                    ax.add_patch(PolygonPatch(poly, ec='r', alpha=0.5, zorder=2))
                for poly in test.wal:
                    ax.add_patch(PolygonPatch(poly, ec='g', alpha=0.5, zorder=2))
                for poly in test.d:
                    ax.add_patch(PolygonPatch(poly, ec='b', alpha=0.5, zorder=2))
                print('done')
                #for i, poly in enumerate(self):
                    #self[i].ImposeData()
                    #r = self[i]#.Substract()
                    #ax.add_patch(PolygonPatch(r, ec='#6699cc', alpha=0.5, zorder=2))
                    #ax.add_patch(PolygonPatch(poly.win, ec='r', alpha=0.5, zorder=2))
                    #ax.add_patch(PolygonPatch(poly.d, ec='g', alpha=0.5, zorder=2))
                    #ax.add_patch(PolygonPatch(poly.wal, ec='b', alpha=0.5, zorder=2))
                ax.axis('scaled')

        self.rooms = []

        for i in range(len(self.room)):
                self.rooms.append(
                    Room({
                    'type': self.ttypes[i],
                    'room': self.room[i]
                    #'walls': self.walls[i],
                    #'windows': self.windows[i],
                    #'doors': self.doors[i]
                })
                )

        print("Ok")

        return RoomList(self.rooms)


##############################################################################


def CntWriter(
    polygon
    ):
    cnt = np.empty(shape=(len(polygon), 2))

    for i, pair in enumerate(polygon):
        cnt[i] = np.array(pair)

    return cnt


def Chopping(
    cnt, 
    coef=10
    ):
    # TODO: Выпрямить линию, соединяющую первую и
    # последнюю линию в списке.
    mem = cnt[0]

    for i in range(len(cnt)-1):
        if i > 0:
            if abs(cnt[i][0] - cnt[i+1][0]) < coef:
                cnt[i+1][0] = cnt[i][0]
            if abs(cnt[i][1] - cnt[i+1][1]) < coef:
                cnt[i+1][1] = cnt[i][1]

        fin = cnt[-1]
    # TODO Убрать нафиг
        if (
            abs(mem[0] - fin[0]) < coef 
            and 
            abs(mem[1] - fin[1]) < coef
            ):
            cnt[-1] = mem

    return cnt


def PaintCnt(
    cnt
    ):
    X = np.take(cnt, indices=0, axis=1)
    Y = np.take(cnt, indices=1, axis=1)
    plt.plot(X, Y)


def Restruct(
    double
    ):
    restruct = []
    sns.set_palette("husl")

    for x in double:
        cur_type, polygon = x
        cnt = CntWriter(polygon)
        cnt = Chopping(cnt)
        #PaintCnt(cnt)
        restruct.append((cur_type, cnt))

    return restruct


##############################################################################


test = Plan('result/plan1.json')
rooms = test.SplitByRooms()
rooms.Apply()#.Show()
#rooms.PlanVis()
#rooms.Show()

for x in rooms:
    x.TB()
    x.LR()
#print(len(test.win))
#rooms.ShowLines()

###############################################################################


def search_eps(line_x, pol, orient, eps=3):
    '''
    Поиск пересечения полигона и прямых, образующих контур
    комнаты в eps-окрестности.
    '''
    for g in range(0, eps):
        if orient == 'horizontal': # для горизонтальных меняем вторую точку
            point1 = (line_x[0][0], line_x[0][1] + g) 
            point2 = (line_x[1][0], line_x[1][1] + g)
            line1 = LineString((point1, point2)) # линия из точек со сдвигом
            if line1.intersects(pol):
                line_x = sum(line_x, ())
                b = pol.bounds
                c1 = b[0] if b[0] > line_x[0] else line_x[0]
                c2 = b[2] if b[2] < line_x[2] else line_x[2]
                proection = ((c1, line_x[1]), (c2, line_x[3])) 

                return proection

            point1 = (line_x[0][0], line_x[0][1] - g) 
            point2 = (line_x[1][0], line_x[1][1] - g)
            line2 = LineString((point1, point2))
            if line2.intersects(pol):
                line_x = sum(line_x, ())
                b = pol.bounds
                c1 = b[0] if b[0] > line_x[0] else line_x[0]
                c2 = b[2] if b[2] < line_x[2] else line_x[2]
                proection = ((c1, line_x[1]), (c2, line_x[3]))

                return proection

        elif orient == 'vertical':
            point1 = (line_x[0][0] + g, line_x[0][1])
            point2 = (line_x[1][0] + g, line_x[1][1])
            line1 = LineString((point1, point2))
            if line1.intersects(pol):
                line_x = sum(line_x, ())
                b = pol.bounds
                # по вертикали снизу мин, сверху макс
                c1 = b[1] if b[1] > line_x[1] else line_x[1]
                c2 = b[3] if b[3] < line_x[3] else line_x[3]
                proection = ((line_x[0], c1), (line_x[2], c2)) 
                return proection

            point1 = (line_x[0][0] - g, line_x[0][1])
            point2 = (line_x[1][0] - g, line_x[1][1])
            line2 = LineString((point1, point2))
            if line2.intersects(pol):
                line_x = sum(line_x, ())
                b = pol.bounds
                c1 = b[1] if b[1] > line_x[1] else line_x[1]
                c2 = b[3] if b[3] < line_x[3] else line_x[3]
                proection = ((line_x[0], c1), (line_x[2], c2)) 
                return proection

        else: 
            return None


def ClearIntersections(points):
    data = []
    h, v = 'horizontal', 'vertical'

    for door in points:
        # как выбрать прямые, которые "соединяются" дверью?
        orient = h if len(door[h]) > len(door[v]) else v
        inter = door[orient]

        if orient == h:
            if len(inter) != 1:
                x_left = max([inter[a][0][0] for a in range(len(inter))])
                x_right = min([inter[a][0][0] for a in range(len(inter))])
            else:
                x_left = inter[0][0][1]
                x_right = inter[0][1][1]

            r = [((x_left, inter[a][0][1]), (x_right, inter[a][1][1])) for a in range(len(inter))]

        elif orient == v:
            if len(inter) != 1:
                x_bottom = max([inter[a][0][1] for a in range(len(inter))])
                x_top = min([inter[a][0][1] for a in range(len(inter))])
            else:
                x_bottom = inter[0][0][1]
                x_top = inter[0][1][1]

            r = [((inter[a][0][0], x_bottom), (inter[a][1][0], x_top)) for a in range(len(inter))]

        data.append(r)

        return data


def SplitData(data):
    """
    Выделяет из списка смешанных координат x-y 
    в отдельные списки по координатам x-y
    """
    data_x = []
    data_y = []
    for dat in data:
        for d in dat:
            print(d)
            data_x.append(d[0][0])
            data_x.append(d[1][0])
            data_y.append(d[0][1])
            data_y.append(d[1][1])
        
    return data_x, data_y

   
def LinesCycle(room, poly, lines_dict, orient):
    """
    Arguments:
        room {[shapely.geometry.polygon.Polygon]} -- [комната, содержащая отрезки стен]
        poly {[__main__.Plan.SplitByRooms.<locals>.Room]} -- [полигон, для которого ищем пересечения]
        lines_dict {[dict]} -- [словарь с отрезками, разделенными по горизонтали/вертикали]
        orient {[str]} -- [ориентация horizontal/vertical]
    
    Returns:
        [type] -- [description]
    """

    sys = {
        'horizontal': [room.top, room.bottom], 
        'vertical': [room.right, room.left]
        }

    for lines_collection in sys[orient]:
        for line in lines_collection:
            l = search_eps(line, poly, orient)
            if l is not None:
                lines_dict[orient].append(l) 
            # записываем координаты отрезка, если пересечение есть
                
    return lines_dict


def GetIntersection(plan_objects):
    """
    Находит прямые из числа образующих стены, которые пересекаются с полигонами
    объектов из переменной plan_objects

    Keyword Arguments:
        plan_objects {[type]} -- [description] (default: {test.d})
    
    Returns:
        [List[Dict[str, List[Point]]]] -- [список словарей(горизонт/вертикаль), содержащих точки пересечения полигонов]
    """
    points = []

    for obj_ind in range(len(plan_objects)): 
        # цикл по объектам на плане
        lines_dict = {'horizontal': [], 'vertical': []}
        # будем записывать в словарь отдельно горизонтальные и 
        # вертикальные прямые, пересекающиеся с объектом
        poly = plan_objects[obj_ind]

        for index in range(len(rooms)): 
            # цикл по комнатам на плане
            room = rooms[index]
            lines_dict = LinesCycle(room, poly, lines_dict, 'horizontal')
            lines_dict = LinesCycle(room, poly, lines_dict, 'vertical')
            
        points.append(lines_dict) 

    return points


"""
gg = GetIntersection(test.win) # получаем список линий, которые пересекаются с проекциями окон/дверей



## окна
window_points = test.data['windows'][0]
window_points = sum(window_points, [])
x = []
y = []
for point in window_points:
    x.append(point[0])
    y.append(point[1])
(x_min, y_min) = (min(x), min(y))
(x_max, y_max) = (max(x), max(y))
(x_center, y_center) = (sum(x)/len(x), sum(y)/len(y))
plt.plot(x, y)


# стены объемные и внутренние контуры
walls_points = test.data['walls'][0]
walls_points = sum(walls_points, [])
x_wall = []
y_wall = []
for point in walls_points:
    x_wall.append(point[0])
    y_wall.append(point[1])
plt.plot(x_wall, y_wall)

room_points = test.data['room'][1]
x_room = []
y_room = []
for point in room_points:
    x_room.append(point[0])
    y_room.append(point[1])
plt.plot(x_room, y_room)

plt.show()
"""