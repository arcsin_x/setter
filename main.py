#!/usr/bin/env python3.6

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image, ImageDraw

from _helpers import color

import config
import clear_cycle as cc
import furniture as furn
import plan_postprocessing_old as ppd

path = 'furniture'

Mebel = furn.LoadFurniture(path)
MebelSet = furn.FurnitureList(Mebel)
MebelSet = MebelSet.Scale()
MebelSet.LoadImages('furniture')

########################################
#test = ppd.Plan('result/plan2.json')
test = ppd.Plan('result/plan1.json')
rooms = test.SplitByRooms()
rooms.Apply()

for x in rooms:
    x.TB()
    x.LR()
#########################################

#ручной ввод типов комнат для плана 2
ttypes_plan2 = ['None', 'None', 'None', 'None', 'bedroom', 'livingroom']
ttypes_plan1 = ['None', 'None', 'bedroom', 'livingroom', 'None', 'None']
ttypes = ttypes_plan1

for i, room in enumerate(rooms):
    if i <= len(ttypes):
        ttype = ttypes[i]
    x = [c[0] for c in room.room]
    y = [c[1] for c in room.room]
    #plt.plot(x, y, label=str(i) + '-я комната')
    if ttype == 'None':
        pass
    else:
        furniture_list = config.PRIORITY[ttype]
        room_walls = {
            'top': room.top, 
            'bottom': room.bottom,
            'left': room.left,
            'right': room.right
        }
        room_walls = cc.placement(mebel_set=MebelSet, room_walls=room_walls, furniture_list=furniture_list)
        #cc.decor(ttype, MebelSet, room_walls, furniture_list)
    config.PLACED_OBJECTS[ttype] = config.TECHINFO
    config.TECHINFO = {}

plt.show()

######Saving plot
#plt.axis('off')
#plt.savefig('result_1.png', dpi=96*14, bbox_inches='tight', pad_inches=0)
#test_plot = Image.open('/home/arcsinx/furniture_setter/result_1.png')

#source = Image.open('/home/arcsinx/Desktop/good_plans/plan2.jpg')
source = Image.open('plan1.jpg')
source = source.transpose(Image.ROTATE_180)
source = source.transpose(Image.FLIP_TOP_BOTTOM)
source = source.transpose(Image.ROTATE_180)

source = source.resize((int(source.size[0]*config.COEF), int(source.size[1]*config.COEF)), Image.ANTIALIAS)

im = source
for room in list(config.PLACED_OBJECTS.keys()):
    for element in list(config.PLACED_OBJECTS[room].keys()):
        #new_test_image = config.PLACED_OBJECTS[room][element]['obj'].new_image
        x_min = min(config.PLACED_OBJECTS[room][element]['bbox'][0])
        x_max = max(config.PLACED_OBJECTS[room][element]['bbox'][0])
        y_min = min(config.PLACED_OBJECTS[room][element]['bbox'][1])
        y_max = max(config.PLACED_OBJECTS[room][element]['bbox'][1])
        correct_y_max = im.size[1]*config.FURN_SCALE - y_min
        correct_y_min = im.size[1]*config.FURN_SCALE - y_max
        if config.PLACED_OBJECTS[room][element]['orient_key'] == 'top':
            new_image = config.PLACED_OBJECTS[room][element]['obj'].image
        elif config.PLACED_OBJECTS[room][element]['orient_key'] == 'left':
            new_image = config.PLACED_OBJECTS[room][element]['obj'].image.transpose(Image.ROTATE_90)
        elif config.PLACED_OBJECTS[room][element]['orient_key'] == 'bottom':
            new_image = config.PLACED_OBJECTS[room][element]['obj'].image.transpose(Image.ROTATE_180)
        elif config.PLACED_OBJECTS[room][element]['orient_key'] == 'right':
            new_image = config.PLACED_OBJECTS[room][element]['obj'].image.transpose(Image.ROTATE_270)
        im.paste(new_image, (int(x_min/config.FURN_SCALE), int(correct_y_min/config.FURN_SCALE)))
        draw = ImageDraw.Draw(im)
        draw.rectangle([(x_min/config.FURN_SCALE, correct_y_min/config.FURN_SCALE), (x_max/config.FURN_SCALE, correct_y_max/config.FURN_SCALE)], outline='red')

im.show()
