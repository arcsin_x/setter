'''
Загрузка данных из results/*.json и отображение результатов.
'''
import numpy as np
import os
import errno
import json
import PIL
from PIL import Image, ImageOps
from typing import List, Dict, Tuple
import matplotlib.pyplot as plt
import matplotlib.image as mpimg


def LoadJson(

    x:str, 
    res_path:str

    ):
    with open(res_path + '/' + x) as f:
        return json.loads(f.read())


def QLoadAll(

    res_path: str = 'result'

    ):
    if os.path.isdir(res_path):
        variaty = [x for x in os.listdir(res_path) if x[-5:] == '.json']
        print(variaty)

        full_list = ([
            dict(
                zip(
                    [x], [LoadJson(x, res_path)]
                    )
                ) 
            for x in variaty
        ])

        return full_list
    else: 
        raise FileNotFoundError(
            errno.ENOENT, os.strerror(errno.ENOENT), res_path
            )


def ObjectsDefiner(

    json_element: Dict[str, Dict[str, List[List[List[int]]]]]
    #это хреново

    ):
    '''
    Структуру исправлять ЗДЕСЬ.
    '''
    data = ({
        'doors_contours': json_element['doors_contours'],
        'window_contours': json_element['window_contours'],
        'wall_contours': json_element['wall_contours']
    })

    colors = ({
        'doors_contours': 'aqua',
        'window_contours': 'red',
        'wall_contours': 'lime'
    })

    result_path = 'graphics_result/'

    return data, colors, result_path


def ContoursDefiner(

    json_element: Dict[str, Dict[str, List[List[List[int]]]]]

    ):
    data = ({
       'rooms_contours': json_element['rooms_contours']
    })

    colors =({
        'rooms_contours':'deeppink'
    }) 

    result_path = 'contours/'

    return data, colors, result_path

def PlotBuilder(

    filename:str,
    result_path:str,
    ext: str,
    data: Dict[str, List[List[List[List]]]], 
    colours: Dict[str, str]

    ):
    '''
    Функция отображает графически стены, окна, двери и контуры комнат
    по набору точек в структуре, определенной в Definer(data).
    '''
    keys = list(data.keys())

    for key in keys:
        print(key)
        for polygon in data[key]:
            print(polygon)
            cnt = np.empty(shape=(len(polygon), 2))
            #polygon = sum(polygon, [])
            print(polygon)

            for i, pair in enumerate(polygon):
                print(pair) 
                cnt[i] = np.array(pair)

            print(cnt)
            X = np.take(cnt, indices=0, axis=1)
            Y = np.take(cnt, indices=1, axis=1)
            plt.plot(X, Y, color = colors[key])

    img = mpimg.imread('images/' + filename[:-4] + ext)

    plt.imshow(img)
    plt.savefig(result_path + filename[:-4] + 'png')
    plt.clf()


if __name__ == '__main__':
    ext = 'jpg'
    full_list = QLoadAll('result')
    for x in full_list:
        filename = list(x.keys())[0]
        print(filename)
        data, colors, result_path = ObjectsDefiner(x[filename])
        PlotBuilder(filename, result_path, ext, data, colors)
        data, colors, result_path = ContoursDefiner(x[filename])
        PlotBuilder(filename, result_path, ext, data, colors)