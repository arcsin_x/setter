'''
Функции циклического и рекурсивного перебора объектов
'''
# TODO PREFERED_PLACE
import config
import utils
from helpers import color
import deco_functions as df

from utils import *
from geometry import *
from check import *
from decorators_test import *

#WallsSorting({'top': [((3297.0, 2681.0), (3458.0, 2681.0)), ((3458.0, 2989.0), (3633.0, 2989.0)), ((3633.0, 3024.0), (4788.0, 3024.0))], 'bottom': [((4788.0, 679.0), (4620.0, 679.0)), ((3689.0, 693.0), (3297.0, 693.0)), ((4620.0, 700.0), (3906.0, 700.0)), ((3906.0, 840.0), (3675.0, 840.0))], 'left': [((3297.0, 693.0), (3297.0, 2681.0)), ((3458.0, 2681.0), (3458.0, 2989.0)), ((3906.0, 700.0), (3906.0, 840.0))], 'right': [((4788.0, 3024.0), (4788.0, 679.0))]})

def WallsSorting(room_walls):
    '''
    Возвращает координаты самого длинного отрезка в 
    списке стен и ориентацию этой стены
    '''
    def check_side(walls_list):
        '''
        Возвращает максимальную длину отрезка стены в
        списке, полученном по ключу ориентации
        '''
        lengths = []

        for i, wall in enumerate(walls_list):
            dx = abs(wall[0][0] - wall[1][0])
            dy = abs(wall[1][0] - wall[1][1])
            max_length = dx if dx > dy else dy
            lengths.append(max_length)

        biggest_wall_on_side_ind = lengths.index(max(lengths))

        return max(lengths), walls_list[biggest_wall_on_side_ind]

    greatest_walls = {}
    for side in list(room_walls.keys()):
        walls_list = room_walls[side]
        max_length, biggest_wall = check_side(walls_list)
        greatest_walls[side] = max_length

    greatest_wall = max_length
    biggest = biggest_wall
    greatest_side = side
    for side in greatest_walls:
        if greatest_walls[side] > greatest_wall:
            greatest_wall = greatest_walls[side]
            greatest_side = side
            biggest = biggest_wall

    print(biggest)
    return biggest, greatest_side

def RecursiveCycle(MebelSet, abstract_object, room_walls, closed_wall, orient_key): #сюда надо передавать тип функции и положение объекта до
    """
    Углубляется рекурсивно в зависимости предыдущего объекта
    """
    if abstract_object not in list(config.DEPENDENCE.keys()):
        print('\tObject ' + color.PURPLE + str(abstract_object) + color.END + ' has no dependened objects')
        return None
    else:
        if not closed_wall:
            print(color.RED + 'ATTENTION:\tРекурсия не вызвана, так как родительский объект' + str(abstract_object) + 'не расположен в комнате\n' + color.END)
            return

        for new_abstract_object in config.DEPENDENCE[abstract_object]:
            dep_type = config.DEPENDENCE[abstract_object][new_abstract_object]
            print(color.PURPLE + '\n!!!!!\tПробуем найти положение объекта '+ str(new_abstract_object) + color.END)
            print('\tI know that placement of', color.YELLOW + str(new_abstract_object) + color.END , 'depends on placement', color.YELLOW + str(abstract_object) + color.END)
            print('\tObject ' + color.YELLOW + str(new_abstract_object) + color.END + ' should be ' + color.GREEN + config.DEPENDENCE[abstract_object][new_abstract_object] + color.END + ' relative to ' + color.YELLOW + str(abstract_object) + color.END)
            new_closed_wall, new_orient_key = CycleWithDeps(MebelSet, new_abstract_object, room_walls, dep_type, closed_wall, orient_key)
            print('Recursive placement function is called from object ' + color.PURPLE + str(abstract_object) + color.END + ' to its dependened objects')
            RecursiveCycle(MebelSet, new_abstract_object, room_walls, new_closed_wall, new_orient_key)

def Cycle(
    MebelSet,
    abstract_object,
    room_walls,
    ):
    """
    Собирает вместе все циклы для определения положения объекта
    """
    horizontal, vertical, length = length_helper()
    place_founded = False
    bbox_collect = []

    concrete_objects = MebelSet.FilterByType(value=abstract_object)
    orient_key, wall_index = find_largest_wall_parameters(room_walls) 
    print(room_walls)
    print(wall_index, orient_key)
    #сначала попробуем поставить у самой длинной стенки
    wall = room_walls[orient_key][wall_index]
    get_length = length[orient_key] 
    #выбираем нужную функцию определения длины

    for concrete_obj in concrete_objects:
        if place_founded:
            break
        if get_length(wall) < concrete_obj.width: #TODO добавить выбор стороны объекта
            pass
        else:
            #переопределяем декорируемую функцию
            @decorators_generator(
                orientation=orient_key, walls=room_walls, obj=concrete_obj,
                wall=wall, place=config.PLACE[abstract_object], corner_side='min'
                )
            def get_coordinates(*args, **kwargs):
                if kwargs['closed_wall']:
                    return kwargs['closed_wall'], kwargs['orient_key'], kwargs['bbox']
                else:
                    return None, None, None

            closed_wall, orient_key, bbox = get_coordinates(
                                        walls=room_walls, bbox=bbox_collect, 
                                        orient_key=orient_key, closed_wall=None
                                        )

            place_founded = True if closed_wall else False

            if place_founded:
                config.FURN[abstract_object] = bbox
                return closed_wall, orient_key
            else:
                return None, None

    #for concrete_obj, orient_key, wall in nested_loop(concrete_objects, room_walls):
    #    get_length = length[orient_key]
#
    #    if place_founded == True:
    #        break

        if get_length(wall) > concrete_obj.width:
            @decorators_generator(
                orientation=orient_key, walls=room_walls, obj=concrete_obj,
                wall=wall, place=config.PLACE[abstract_object], corner_side='min'
                )
            def get_coordinates(*args, **kwargs):
                if kwargs['closed_wall']:
                    return kwargs['closed_wall'], kwargs['orient_key'], kwargs['modified_walls']
                else:
                    return None, None

            closed_wall, orient_key, modified_walls = get_coordinates(
                                        walls=room_walls, bbox_collect=bbox_collect, 
                                        orient_key=orient_key, closed_wall=None, modified_walls=None
                                        )

            place_founded = True if closed_wall else False
            if modified_walls:
                ind = room_walls[orient_key].index(wall)
                room_walls[orient_key].remove(room_walls[orient_key][ind])
                room_walls[orient_key].append(modified_walls[0])
                return closed_wall, orient_key
        """
            coordinates = place_object(wall, orient_key, concrete_obj, gen_place = config.PLACE[abstract_object])
            if check_min_distance(coordinates):
                print(color.GREEN + '\t\tПроверка дистанции пройдена успешно' + color.END)
                #add_object
                config.FURN[concrete_obj.ttype] = coordinates
                room_walls, closed_wall = change_walls(room_walls, orient_key, wall, coordinates)
                print('\n################################################################################################\nObject', color.PURPLE + str(abstract_object) + color.END, 'successfully placed in room\non wallside', color.PURPLE + str(orient_key) + color.END, 'with line coordinates', color.PURPLE + str(wall) + color.END, '\nand bbox coordinates', color.PURPLE + str(coordinates) + color.END, '\n################################################################################################\n')
                break
                print(color.RED + 'RECYCLE:\tПроверка дистанции провалена' + color.END)
            else:
                #print(color.RED + str(wall) + ' has no enough free space (' + str(concrete_obj.width) + ')' + color.END)
                pass
        else:
            print(color.RED + '\n################################################################################################\nATTENTION:\tObject ' + str(abstract_object) + ' didnt placed in room\n################################################################################################\n' + color.END)
            return None, None
    else: 
        for concrete_obj, orient_key, wall in nested_loop(concrete_objects, room_walls):
            if check_free_space(wall, orient_key, concrete_obj): # TODO проверка должна быть усложнена
                coordinates = place_object(wall, orient_key, concrete_obj, gen_place = config.PLACE[abstract_object])
                #checks by coordinates
                if check_min_distance(coordinates):
                    room_walls, closed_wall = ...
                    print(color.GREEN + '\t\tПроверка дистанции пройдена успешно' + color.END)
                    #add_object
                    config.FURN[concrete_obj.ttype] = coordinates
                    room_walls, closed_wall = change_walls(room_walls, orient_key, wall, coordinates)
                    print('\n################################################################################################\nObject', color.PURPLE + str(abstract_object) + color.END, 'successfully placed in room\non wallside', color.PURPLE + str(orient_key) + color.END, 'with line coordinates', color.PURPLE + str(wall) + color.END, '\nand bbox coordinates', color.PURPLE + str(coordinates) + color.END, '\n################################################################################################\n')
                    break
                print(color.RED + 'RECYCLE:\tПроверка дистанции провалена' + color.END)
            else:
                #print(color.RED + str(wall) + ' has no enough free space (' + str(concrete_obj.width) + ')' + color.END)
                pass
        else:
            print(color.RED + '\n################################################################################################\nATTENTION:\tObject ' + str(abstract_object) + ' didnt placed in room\n################################################################################################\n' + color.END)
            return None, None
"""

#TODO КАК ЭТО объединить с простым циклом?
def CycleWithDeps(
    MebelSet,
    abstract_object,
    room_walls,
    dep_type,
    closed_wall,
    orient_key
    ):
    """
    Собирает вместе все циклы для определения положения объекта
    """
    bbox_collect = []
    print(color.GREEN + '*****\tTrying to place object ' + str(dep_type) + ' relative to ' + str(orient_key) + ' : ' + str(closed_wall) + color.END)
    concrete_objects = MebelSet.FilterByType(value=abstract_object)
    for concrete_obj in concrete_objects:
        if dep_type == 'near':
            left = gmt.get_line_with_point(room_walls[orient_key], closed_wall[0][0], closed_wall[0][1])
            right = gmt.get_line_with_point(room_walls[orient_key], closed_wall[1][0], closed_wall[1][1])
            horizontal, vertical, length = length_helper()
            get_length = length[orient_key]

            if left and get_length(left) > concrete_obj.width:
                @decorators_generator(
                orientation=orient_key, #зависит от типа стены
                walls=room_walls,
                obj=concrete_obj,
                wall=closed_wall,
                place='wall_corner', #зависит от типа объекта
                corner_side='max'
                )
                def get_coordinates(*args, **kwargs):
                    if kwargs['closed_wall']:
                        print(kwargs['closed_wall'])
                        print(kwargs['orient_key'])
                        print(kwargs['bbox_collect'])
                        return kwargs['closed_wall'], kwargs['orient_key'], kwargs['modified_walls']
                    else:
                        return None, None, None
                closed_wall, orient_key, modified_walls = get_coordinates(
                                                walls=room_walls, bbox_collect=bbox_collect, 
                                                orient_key=orient_key, closed_wall=None,
                                                modified_walls=None
                                                )
                place_founded = True if closed_wall else False
                if modified_walls:
                    if closed_wall in room_walls[orient_key]:
                        ind = room_walls[orient_key].index(closed_wall)
                        room_walls[orient_key].remove(room_walls[orient_key][ind])
                        room_walls[orient_key].append(modified_walls[0])
                        return closed_wall, orient_key
                    else:
                        return None, None
                else:
                    if right and get_length(right) > concrete_obj.width:
                        @decorators_generator(
                        orientation=orient_key, #зависит от типа стены
                        walls=room_walls,
                        obj=concrete_obj,
                        wall=wall,
                        place='wall_corner', #зависит от типа объекта
                        corner_side='min'
                        )
                        def get_coordinates(*args, **kwargs):
                            if kwargs['closed_wall']:
                                print(kwargs['closed_wall'])
                                print(kwargs['orient_key'])
                                print(kwargs['bbox_collect'])
                                return kwargs['closed_wall'], kwargs['orient_key'], kwargs['modified_walls']
                            else:
                                return None, None, None
                        closed_wall, orient_key, modified_walls = get_coordinates(
                                                        walls=room_walls, bbox_collect=bbox_collect, 
                                                        orient_key=orient_key, closed_wall=None,
                                                        modified_walls=None
                                                        )
                        place_founded = True if closed_wall else False
                        if modified_walls:
                            ind = room_walls[orient_key].index(wall)
                            room_walls[orient_key].remove(room_walls[orient_key][ind])
                            room_walls[orient_key].append(modified_walls)
                            return closed_wall, orient_key
                        else:
                            return None, None
    return None, None

"""
            wall, side = df.relations_dict[dep_type](room_walls, orient_key, closed_wall)
            if check_free_space(wall, orient_key, concrete_obj): # TODO проверка должна быть усложнена
                coordinates = place_object(wall, orient_key, concrete_obj, side, gen_place='wall_corner') # WALL для рекурсивного вызова заменяется на положение объекта ДО
                config.FURN[concrete_obj.ttype] = coordinates
                room_walls, closed_wall = change_walls(room_walls, orient_key, wall, coordinates)
                print('\n################################################################################################\nObject', color.PURPLE + str(abstract_object) + color.END, 'successfully placed in room\non wallside', color.PURPLE + str(orient_key) + color.END, 'with line coordinates', color.PURPLE + str(wall) + color.END, '\nand bbox coordinates', color.PURPLE + str(coordinates) + color.END, '\n################################################################################################\n')
                break
            else:
                print(color.RED + str(wall) + ' has no enough free space (' + str(concrete_obj.width) + ')' + color.END)
        elif dep_type == 'opposite':
            wall, orient_key = df.relations_dict[dep_type](room_walls, orient_key, closed_wall)
            if not wall:
                print(color.RED + '\n################################################################################################\nATTENTION:\tObject ' + str(abstract_object) + ' didnt placed in room(opposite)\n################################################################################################\n' + color.END)
            else:
                if check_free_space(wall, orient_key, concrete_obj): # TODO проверка должна быть усложнена
                    coordinates = place_object(wall, orient_key, concrete_obj, gen_place='wall_center') # WALL для рекурсивного вызова заменяется на положение объекта ДО
                    config.FURN[concrete_obj.ttype] = coordinates
                    room_walls, closed_wall = change_walls(room_walls, orient_key, wall, coordinates)
                    print('\n################################################################################################\nObject', color.PURPLE + str(abstract_object) + color.END, 'successfully placed in room\non wallside', color.PURPLE + str(orient_key) + color.END, 'with line coordinates', color.PURPLE + str(wall) + color.END, '\nand bbox coordinates', color.PURPLE + str(coordinates) + color.END, '\n################################################################################################\n')
                    break
                else:
                    print(color.RED + str(wall) + ' has no enough free space (' + str(concrete_obj.width) + ')' + color.END)
    else:
        print(color.RED + 'Object ' + str(abstract_object) + ' didnt placed in room' + color.END)

    return closed_wall, orient_key
"""